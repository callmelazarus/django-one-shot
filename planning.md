## Feature 1

* [x] Fork and clone the starter project from django-one-shot 
* [x] Create a new virtual environment in the repository directory for the project
  * [x] python -m venv .venv 

* [x] Activate the virtual environment
  * [x] source .venv/bin/activate 
* [x] Upgrade pip
  * [x] pip install --upgrade pip
* [x] Install django
  * [x] pip install django
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8
* [x] Install djlint
  * [x] pip install djlint
* [x] Install debug toolbar
  * [x] pip install django-debug-toolbar
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt

## Feature 2

* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
  * [x] django-admin startproject brain_two .     
* [x] Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
  * [x] python manage.py startapp todos
  * [x] install in INSTALLED APPS in settings.py
  * [x] 'todos.apps.TodosConfig',

* [x] Run the migrations
  * [x] python manage.py makemigrations
  * [x] python manage.py migrate
* [x] Create a super user
  * [x] python manage.py createsuperuser

## Feature 3
Create model
started to transfer notes to Notion because it was more organized there:

https://balanced-apple-83d.notion.site/Django-one-shot-Project-34b61d86f86c43e9a4e787c1c7473786

## Feature 4
register model TodoList in admin

## Feature 5
Create todoitem

## Feature 6
register model TodoItem in admin

## Feature 7
view function
path work in projects and app
html template - with tables

## Feature 8
challenging one.
took a break, experimented
looked at example code
- added list features to be able to link to specific pages, and show number of tasks
- added detail page

## Feature 9
tricky -> redirection to the page you just created
you had to save the form in a new variable, and us the pk of that variable in the 2nd argument of the redirect

- created create page
- linked create page to list page

## Feature 10
tricky part was getting the url tag correct, making sure second argument, took the context_key.pk
redirect also needed second argument, pk=pk to go to the updated detail page
- created update/edit page

## Feature 11
- added delete view funciton