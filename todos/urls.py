from django.urls import path
from todos.views import delete_todolist, show_a_todolist, show_todolist, create_todolist, update_todolist, delete_todolist, create_todoitem, update_todoitem

urlpatterns = [
    path("", show_todolist, name="todo_list_list"),
    path("<int:pk>/", show_a_todolist, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:pk>/edit/", update_todolist, name="todo_list_update"),
    path("<int:pk>/delete/",delete_todolist, name="todo_list_delete"),
    path("items/create/",create_todoitem, name="todo_item_create"),
    path("items/<int:pk>/edit/",update_todoitem, name="todo_item_update"),
]