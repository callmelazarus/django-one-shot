from django.shortcuts import render, redirect, reverse
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListUpdateForm, TodoItemForm, TodoItemUpdateForm


def show_todolist(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "todos/list.html", context)


def show_a_todolist(request, pk):
    todolist = TodoList.objects.get(pk=pk)
    tasks = (
        todolist.items.all()
    )  # getting the specific tasks/items for that todolist
    context = {
        "todolist": todolist,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_form = form.save()
            return redirect(
                "todo_list_detail",
                pk=new_form.pk  # will redirect you to created form
            )
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_todolist(request, pk):
    model_instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoListUpdateForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", pk=pk)
    else:
        form = TodoListUpdateForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todos/updatelist.html", context)


def delete_todolist(request, pk):
    model_instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/deletelist.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            get_list_pk = form.save()
            # for redirect - need to figure out how to get pk of the related todolist
            # how I see it. get_list_pk is an instance of Todoitem. list is the attribute
            return redirect("todo_list_detail", pk=get_list_pk.list.pk)
                
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/createitem.html", context)


def update_todoitem(request, pk):
    model_instance = TodoItem.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoItemUpdateForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", pk=pk)
    else:
        form = TodoItemUpdateForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todos/updateitem.html", context)